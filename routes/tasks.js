const express = require('express');
const router = express.Router();
// const prisma = require('../prisma.service');
const { PrismaClient } = require('@prisma/client');
const { task } = new PrismaClient();



router.post('/',async(req,res) =>{
    const { content, color} = req.body;
    const newTask = await task.create({
        data:{
            content,
            color
        }
    }); 
    res.json(newTask)

});

router.get('/',async(req,res) =>{
    const tasks = await task.findMany({
        select:{
            id: true,
            content: true,
            color: true,
        }
    });
    res.json(tasks);

});

router.delete('/:id', async (req, res) => {
    let { id } = req.params;
    id = parseInt(id);
    const deleteTask = await task.delete({
      where: {
        id,
      }
    });
    res.json(deleteTask);
  });



module.exports = router;