const express = require('express');
const app = express();
const tasksRouter = require('./routes/tasks');
const logger = require('./middleware/logger');



app.use(express.json());
app.use('/main',tasksRouter);

app.use(express.urlencoded({extended: false}));

app.use(logger);

app.get('/',(req,res) => {
    res.send('<h1>Hello express</h1>');
});

app.listen(3000,() => {
    console.log('Listening to port 3000');
});